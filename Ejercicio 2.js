//Crear un metodo constructor llamada persona. Sus atributos son: nombre, edad y cedula. Construye los siguientes métodos para la clase:
//1.1 mostrar(): Muestra los datos de la persona. 
//1.2 es_mayor_de_edad(): Devuelve un valor lógico indicando si es mayor de edad.


function persona(nom_persona,edad_persona,ID_persona){
    this.nombre=nom_persona;
    this.edad=edad_persona;
    this.cedula=ID_persona;
    this.es_mayor_de_edad = function(){
        if (this.edad>=18){
            return true;
        }else{
            return false;
        }
    }
    this.mostrar = function(){
      var nom_aux = prompt("Introduzca nombre: ");
      var edad_aux = parseInt(prompt("Introduzca edad: "));
      var cedula_aux = prompt("Introduzca cedula: ");

      let persona_aux = new persona(nom_aux,edad_aux,cedula_aux);
      let mostrar = (`Nombre: ${nom_aux} / Edad: ${edad_aux} / Cedula: ${cedula_aux}`);

      console.log(mostrar);
      console.log(persona_aux.es_mayor_de_edad());

    }
}

//Crea un metodo constructor llamado cuenta que tendrá los siguientes atributos: titular (que es nombre de la persona) y cantidad. 
//El titular será obligatorio y la cantidad es opcional. Construye los siguientes métodos para el metodo:

//2.1 mostrar(): Muestra los datos de la cuenta. 
//2.2 ingresar(cantidad): se ingresa una cantidad a la cuenta, si la cantidad introducida es negativa, no se hará nada. 
//2.3 retirar(cantidad): se retira una cantidad a la cuenta. La cuenta puede estar en números rojos.

function cuenta(){
  this.titular;
  this.cantidad;
  this.crear_cuenta = function(){
    let nombre = prompt("ingresa el nombre del titular: ");
    let cantidad_inicial = parseInt(prompt("ingresa la cantidad inicial de la cuenta: "));
    if(cantidad_inicial < 0){
      return "Error el saldo inicial tiene que ser mayor a 0";
    }else{
      this.titular = nombre;
      this.cantidad = cantidad_inicial;
    }
    
  }
  this.mostrar = function(){
    return `Su saldo actual es de: $${this.cantidad}`;
  }

  this.ingresar = function(){
    let valor = prompt("Ingresa el valor que se agregara a tu cuenta");
    this.cantidad = this.cantidad + parseInt(valor);
    return `Se agrego a tu cuenta $${valor} y ahora tienes un saldo de: $${this.cantidad}`;    
  }

  this.retirar = function(){
    let valor = prompt("Ingresa el valor que se va a retirar");
    this.cantidad = this.cantidad - parseInt(valor);
    return `Se retiró de tu cuenta $${valor} y ahora tienes un saldo de: $${this.cantidad}`;    
  }
}

//Crear un metodo constructor llamado formulas. Construye los siguiente metodos para la clase:
//3.1 sumar(entero, entero) 
//3.2 fibonacci(cantidad) a partir de una entero sacar los numeros 
//3.3 operacion_modulo(cantidad) a partir de una cantidad mostrar cuales dan residuo 0 
//3.4 primos(cantidad) a partir de una cantidad mostrar cuales son numeros primos

function formulas(){
    this.sumar = function(){
        let valor1 = prompt("Ingresa el primer valor");
        let valor2 = prompt("Ingresa el segundo valor");
        this.cantidad = parseInt(valor1) + parseInt(valor2);
        return `El primer valor es: ${valor1} / El segundo valor es: ${valor2} y la suma de ambos es: ${this.cantidad}`;
    }

    this.fibonacci = function(numero){
        fibo_total = 0;
        for (let i = 1; i <= numero; i++){
            fibo_total = fibo_total + i;
        }
        return fibo_total;
    }

    this.operacion_modulo = function(){
        let num = parseInt(prompt("Ingrese numero: "));

        for(let i=1;i<=num;i++){

            if (i%2 == 0 ){
                console.log("El numero: "+i+ " da residuo 0")
            }
        }
    }

    this.primos = function(){
        let num = parseInt(prompt("Ingrese numero: "));

        for(let i=1;i<=num;i++){
            if (i%2 != 0)
            console.log("El numero: "+i+ " es primo")
        }
    }
}

//Crear un metodo constructor llamado persona. Sus atributos son: nombre, edad, DNI, sexo (H hombre, M mujer), peso y altura Construye los siguiente metodos para la clase:
//4.1 calcularIMC(): calculara si la persona esta en su peso ideal (peso en kg/(altura^2 en m)), si esta fórmula devuelve un valor menor que 20, la función devuelve un -1, 
//si devuelve un número entre 20 y 25 (incluidos), significa que esta por debajo de su peso ideal 
//la función devuelve un 0 y si devuelve un valor mayor que 25 significa que tiene sobrepeso, la función devuelve un 
//1. Te recomiendo que uses constantes para devolver estos valores. 

//4.2 esMayorDeEdad(): indica si es mayor de edad, devuelve un booleano. 
//4.3 comprobarSexo(char sexo): comprueba que el sexo introducido es correcto. Si no es correcto, sera H.

function persona(nombre_persona,edad_persona,DNI_persona,sexo_persona,peso_persona,altura_persona){
    this.nombre = nombre_persona;
    this.edad = edad_persona;
    this.DNI = DNI_persona;
    this.sexo = sexo_persona;
    this.peso = peso_persona;
    this.altura = altura_persona;
    this.calcularIMC = function(){
        const menor_20 = -1;
        const entre_20_25 = 0;
        const sobrepeso = 1;

        this.operacion = this.peso / (this.altura^2)
        if(this.operacion < 20){
            return menor_20;
        }else if(this.operacion >= 20 && this.operacion <= 25){
            return entre_20_25;
        }else if (this.operacion >25 ){
            return sobrepeso;
        }
    }

    this.es_mayor_de_edad = function(){
        if (this.edad>=18){
            return true;
        }else{
            return false;
        }
    }

    this.comprobarSexo = function(){
        if (this.sexo == "M"){
            return `Es correcto, su sexo es ${this.sexo}`
        }else if (this.sexo == "H"){
            return `No es correcto, su sexo es ${this.sexo}`
        }
    }
}

//crear un metodo constructor llamado contraseña. Sus atributos longitud y contraseña Construye los siguiente metodos para la clase:
//5.1 esFuerte(): devuelve un booleano si es fuerte o no, para que sea fuerte debe tener mas de 2 mayúsculas, mas de 1 minúscula y mas de 5 números. 
//5.2 generarPassword(): genera la contraseña del objeto con la longitud que tenga. 
//5.3 seguridadPaswword(); indicar si la contraseña es debil contiene entre 1 a 6 caracteres solo numeros, 
//media (7 a 10 caracteres numeros y letras) o fuerte (11 a 20 caracteres letras y caracteres especiales)

function metodo_contrasena (contrasena,longitud){
    this.contrasena = contrasena;
    this.longitud = parseInt(longitud);
    this.esFuerte = function (){
      let mayusculas = 0;
      let minuscula = 0;
      let num = 0;

      for(var i = 0; i < this.longitud; i++){
        if(this.contrasena[i] == this.contrasena[i].toUpperCase() && this.contrasena[i] != parseInt(this.contrasena[i]) && /[A-Za-z0-9]/.test(this.contrasena[i]) != false){
          mayusculas++;

        }if (this.contrasena[i] == this.contrasena[i].toLowerCase() && this.contrasena[i] != parseInt(this.contrasena[i]) && /[A-Za-z0-9]/.test(this.contrasena[i]) != false){
          minuscula++;

        }if(this.contrasena[i] == parseInt(this.contrasena[i]) && /[A-Za-z0-9]/.test(this.contrasena[i]) != false){
          num++;

        }
      }
      if (mayusculas>2 && minuscula >1 && num > 5){
        return true;
      }else{
        return false;
      }
   } 

   this.generarPassword = function(){
      let new_contrasena = Math.random().toString(36).substr(2, this.longitud)
      console.log(`Contraseña nueva: ${new_contrasena}`)
   }
   
   this.seguridadPaswword = function(){
    let letras = 0
    let num = 0
    let especiales = 0

    for(var i = 0; i < this.longitud; i++){
        if(this.contrasena[i] == this.contrasena[i] && this.contrasena[i] != parseInt(this.contrasena[i]) && /[A-Za-z0-9]/.test(this.contrasena[i]) != false){
           letras++ 
        }

        if(this.contrasena[i] == parseInt(this.contrasena[i]) && /[A-Za-z0-9]/.test(this.contrasena[i]) != false){
            num++
        }

        if(/[A-Za-z0-9]/.test(this.contrasena[i]) == false){
            especiales++
        }
    }

    if(this.longitud <= 6 ){
        console.log("Contraseña debil")
    } else if(this.longitud >= 7 && this.longitud <= 10) {
        console.log("Contraseña media")
    } else {
        console.log("Contraseña fuerte")
    }
  }
}

//Implementar un objeto que modele un contador. Un contador se puede incrementar o decrementar, recordando el valor actual. Al resetear un contador, se pone en cero.
//Además es posible indicar directamente cual es el valor actual. Este objeto debe entender 
//los siguientes mensajes: 6.1 reset() 6.2 inc() 6.3 dec() 6.4 valorActual() 6.5 valorActual(nuevoValor) 
//P.ej. si se evalúa la siguiente secuencia contador.valorActual(10) contador.inc() contador.inc() contador.dec() contador.inc() contador.valorActual() el resultado debe ser 12.

function contador(){
    this.valor = 0;

    this.valor_Actual = function(cantidad){
        if(cantidad == null){
            console.log(`El valor actual es ${this.valor}`);
        }else{
            this.valor = parseInt(cantidad);
            this.type = "Valor actual"
        }
    }

    this.incrementar = function(){
        this.valor++;
        this.type = "Incremento actual"
    }

    this.decrementar = function(){
        this.valor--;
        this.type = "Decremento actual"
    }

    this.reset = function(){
        this.valor = 0;
        this.type = "Reset"
    }
}


//Agregar al contador del ejercicio 6, la capacidad de recordar un String que representa el último comando que se le dio. 
//Los Strings posibles son "reset", "incremento", "decremento" o "actualizacion" (para el caso de que se invoque valorActual con un parámetro). 
//Para saber el último comando, se le envía al contador el mensaje ultimoComando().
//En el ejemplo del ejercicio 3, si luego de la secuencia indicada se evalúa contador.ultimoComando() el resultado debe ser "incremento"

contador.prototype.ultCom = function (){
    return `Ultimo comando es: ${this.type}`
}

//Implementar un objeto que modele a Chimuela, una dragona de la que nos interesa saber qué energía tiene en cada momento, medida en joules. 
//En el metodo constructor simpli�cado que nos piden implementar, 
//las únicas acciones que vamos a contemplar son: cuando Chimuela come una cantidad de comida especi�cada en gramos, en este caso adquiere 4 joules por cada gramo,
// y cuando Chimuela vuela una cantidad de kilómetros, en este caso gasta un joule por cada kilómetro, más 10 joules de �costo �jo� de despegue y aterrizaje.
// La energía de Chimuela nace en 0. El objeto que implementa este metodo constructor de Chimuela, debe entender los siguientes mensajes: 
//8.1 comer(gramos) 
//8.2 volar(kilometros) 
//8.3 energia() 
//P.ej. si sobre un REPL(Read-Eval-Print-Loop)(Lectura-Evaluación-Impresión) recién lanzado se evalúa 
//la siguiente secuencia Chimuela.comer(100) Chimuela.volar(10) Chimuela.volar(20) Chimuela.energia() el resultado debe ser 350.

function dragon_chimuela(){
    this.cant_energia = 0;
    this.comer = function(dar_comida){
        this.cant_energia = 4 * dar_comida;
    }

    this.volar = function(kilometros){
        let despegue_aterrizaje = 10;
        if(this.cant_energia == 0 || (this.cant_energia - parseInt(kilometros) - despegue_aterrizaje)< 0){
            return `Chimuela no volara mas, no hay energia`
        }else{
            this.cant_energia = this.cant_energia - parseInt(kilometros) - despegue_aterrizaje;
        }
    }
    this.energia = function(){
        return `Chimuela tiene una energia actual de: ${this.cant_energia}`
    }
}

//Agregar al metodo constructor de Chimuela del ejercicio 8, la capacidad de entender estos mensajes: estaDebil(), Chimuela está débil si su energía es menos de 50. estaFeliz(), 
//Chimuela está feliz si su energía está entre 500 y 1000. cuantoQuiereVolar(), que es el resultado de la siguiente cuenta. De base, quiere volar (energía / 5) kilómetros, 
//p.ej., si tiene 120 de energía, quiere volar 24 kilómetros. Si la energía está entre 300 y 400, entonces hay que sumar 10 a este valor, y si es múltiplo de 20, otros 15. 
//Entonces, si Chimuela tiene 340 de energía, quiere volar 68 + 10 + 15 = 93 kilómetros. Para probar esto, sobre un REPL recién lanzado darle de comer 85 a Chimuela, así la energía queda en 340

dragon_chimuela.prototype.estaDebil = function(){
    if(this.cant_energia < 50){
        console.log(`La dragona tiene una energia de: ${this.cant_energia}, por lo tanto esta debil`)
    }else{
        console.log(`La dragona tiene una energia de: ${this.cant_energia}, no esta debil`)
    }
}

dragon_chimuela.prototype.estaFeliz = function(){
    if(this.cant_energia >= 500 && this.cant_energia <= 1000){
        console.log(`La dragona tiene una energia de: ${this.cant_energia}, ella esta feliz`)
    }else{
        console.log(`La dragona tiene una energia de: ${this.cant_energia}, ella esta triste`)
    }
}

dragon_chimuela.prototype.cuantoQuiereVolar = function(){
    let quiereVolar = this.cant_energia / 5

    if(this.cant_energia>= 300 && this.cant_energia <= 400){
        quiereVolar = quiereVolar + 10
    } else if(this.cant_energia >= 300 && this.cant_energia <= 400){
        quiereVolar = quiereVolar + 10
    }

    if(this.cant_energia % 20 == 0){
        quiereVolar = quiereVolar +15
        console.log(`Chimuela quiere volar ${quiereVolar} Km mas :D`)
    } else {
        console.log(`Chimuela quiere volar ${quiereVolar} Km mas :D`)
    }
}

//Implementar un objeto que represente una calculadora sencilla, que permita sumar, restar y multiplicar. Este objeto debe entender los siguientes mensajes: 
//10.1 cargar(numero) 
//10.2 sumar(numero) 
//10.3 restar(numero) 
//10.4 multiplicar(numero) 
//10.5 valorActual() P.ej. si se evalúa la siguiente secuencia calculadora.cargar(0) calculadora.sumar(4) calculadora.multiplicar(5) calculadora.restar(8) calculadora.multiplicar(2) 
//calculadora.valorActual() el resultado debe ser 24.

function calculadora(){
    this.num = 0
    this.cargar = function(valor){
        this.num = valor

    this.sumar = function(valor){
        this.num = this.num + valor
    }

    this.restar = function(valor){
        this.num = this.num - valor
    }

    this.multiplicar = function(valor){
        this.num = this.num * valor
    }

    this.valorActual = function(){
        console.log(`El valor actual es: ${this.num}`)
    }
  }
}

//Crear un metodo constructor llamado Libro. Sus atributos título del libro, autor, número de ejemplares del libro y número de ejemplares prestados los siguiente metodos para la clase:

//préstamo() que incremente el atributo correspondiente cada vez que se realice un préstamo del libro. No se podrán prestar libros de los que no queden ejemplares disponibles para prestar. 
//Devuelve true si se ha podido realizar la operación y false en caso contrario. 
//devolucion() que decremente el atributo correspondiente cuando se produzca la devolución de un libro. No se podrán devolver libros que no se hayan prestado. 
//Devuelve true si se ha podido realizar la operación y false en caso contrario. 
//toString() para mostrar los datos de los libros.

function libro(titulo_Libro, autor_Libro, ejemplares_Libro, ejemplares_prestados){
    this.titulo = titulo_Libro;
    this.autor = autor_Libro;
    this.ejemplares = parseInt(ejemplares_Libro);
    this.prestados = parseInt(ejemplares_prestados);

    this.prestamo = function(valor){
        if(this.ejemplares > 0){
            this.ejemplares--;
            this.prestados++;
            return true;
        } else {
            return false;
        }
    }

    this.devolucion = function(valor){
        if(this.prestados > 0){
            this.ejemplares++
            this.prestados--
            return true;
        } else {
            return false;
        }
    }

    this.toString = function(){
        console.log("==== DATOS DE LIBROS ====")
        console.log("")
        console.log(`Titulo = ${this.titulo}`)
        console.log(`Autor = ${this.autor}`)
        console.log(`Ejemplares del libro = ${this.ejemplares}`)
        console.log(`Ejemplares prestados = ${this.prestados}`)
    }
}

//Se está pensando en el diseño de un juego que incluye la nave espacial Enterprise. En el juego, esta nave tiene un nivel de potencia de 0 a 100, y un nivel de coraza de 0 a 20. 
//La Enterprise puede encontrarse con una pila atómica, en cuyo caso su potencia aumenta en 25. 
//encontrarse con un escudo, en cuyo caso su nivel de coraza aumenta en 10. 
//recibir un ataque, en este caso se especi�can los puntos de fuerza del ataque recibido. 
//La Enterprise �para� el ataque con la coraza, y si la coraza no alcanza, el resto se descuenta de la potencia. 
//P.ej. si la Enterprise con 80 de potencia y 12 de coraza recibe un ataque de 20 puntos de fuerza, puede parar solamente 12 con la coraza, los otros 8 se descuentan de la potencia. 
//La nave debe quedar con 72 de potencia y 0 de coraza. 
//Si la Enterprise no tiene nada de coraza al momento de recibir el ataque, entonces todos los puntos de fuerza del ataque se descuentan de la potencia. 
//La potencia y la coraza tienen que mantenerse en los rangos indicados, 
//p.ej. si la Enterprise tienE 16 puntos de coraza y se encuentra con un escudo, entonces queda en 20 puntos de coraza, no en 26. 
//Tampoco puede quedar negativa la potencia, a lo sumo queda en 0. 
//La Enterprise nace con 50 de potencia y 5 de coraza. Implementar este metodo constructor de la Enterprise, que tiene que entender los siguientes mensajes: 
//12.1 potencia() 12.2 coraza() 12.3 encontrarPilaAtomica() 12.4 encontrarEscudo() 12.5 recibirAtaque(puntos)

function Enterprise(){
    this.potencia_valor = 50
    this.coraza_valor= 5

    this.potencia = function(){
        console.log(`Enterprise potencia actual = ${this.potencia_valor}`)
    }

    this.coraza = function(){
        console.log(`Enterprise escudo actual = ${this.coraza_valor}`)
    }

    this.encontrarPilaAtomica = function(){
       if (this.potencia_valor > 0){
          if(this.potencia_valor + 25 > 100){
            this.potencia_valor = 100
          } else {
            this.potencia_valor = this.potencia_valor + 25
          }
       } else {
         console.log('GAME OVER, no se puede agregar mas potencia')
       }
    }

    this.encontrarEscudo = function(){
        if (this.potencia_valor > 0){
            if(this.coraza_valor + 10 > 20){
              this.coraza_valor = 20
            } else {
              this.coraza_valor = this.coraza_valor + 10
            }
        } else {
         console.log('GAME OVER, no se le puede agregar mas escudo')
       }
    }

    this.recibirAtaque = function(puntos_ataque){
        this.valor_golpe = puntos_ataque
        if (this.potencia_valor > 0){
            if(this.coraza_valor - this.valor_golpe >= 0){
                this.coraza_valor = this.coraza_valor - this.valor_golpe
            } else if (this.coraza_valor - this.valor_golpe < 0){
                this.valor_golpe = this.valor_golpe - this.coraza_valor
                this.coraza_valor = 0
                if(this.potencia_valor - this.valor_golpe > 0){
                    this.potencia_valor = this.potencia_valor - this.valor_golpe
                } else {
                    this.coraza_valor = 0
                    this.potencia_valor = 0
                    console.log('La Enterprise ha muerto')
                    console.log(`Potencia = ${this.potencia_valor}`)
                    console.log(`Coraza = ${this.coraza_valor}`)
                }
            }
        } else {
            this.coraza_valor = 0
            this.potencia_valor = 0
            console.log('GAME OVER')
            console.log(`Potencia final = ${this.potencia_valor}`)
            console.log(`Coraza final = ${this.potencia_valor}`)
        }
    }
}

//Agregar al metodo constructor de la Enterprise del ejercicio 12, la capacidad de entender estos mensajes. fortalezaDefensiva(), que es el máximo nivel de ataque que puede resistir, 
//o sea, coraza más potencia. necesitaFortalecerse(), tiene que ser true si su coraza es 0 y su potencia es menos de 20. fortalezaOfensiva(), que corresponde a 
//cuántos puntos de fuerza tendría un ataque de la Enterprise. Se calcula así: si tiene menos de 20 puntos de potencia entonces es 0, si no es (puntos de potencia - 20) / 2.

Enterprise.prototype.fortalezaDefensiva = function(){
    this.maxResistencia = this.potencia_valor + this.coraza_valor
    console.log(`Enterprise puede recibir un maximo de ataque de = ${this.maxResistencia}`)
}

Enterprise.prototype.necesitaFortalecerse = function(){
    if(this.coraza_valor == 0 && this.potencia_valor < 20){
        console.log(true)
    }
}

Enterprise.prototype.fortalezaOfensiva = function(){
    this.fuerza_ataque_enterprise = 0
    if(this.potencia_valor >= 20){
        this.fuerza_ataque_enterprise = (this.potencia_valor - 20) / 2
        console.log(`Fuerza de ataque actual: ${this.fuerza_ataque_enterprise}`)
    }else{
        console.log(`Fuerza de ataque actual: ${this.fuerza_ataque_enterprise}`)
    }
}

//Un taller de diseño de autos quiere estudiar un nuevo prototipo. Para eso, nos piden hacer un metodo constructor concentrado en las características del motor. 
//El prototipo de motor tiene 5 cambios (de primera a quinta), y soporta hasta 5000 RPM. La velocidad del auto se calcula así: (rpm / 100) * (0.5 + (cambio / 2)). 
//P.ej. en tercera a 2000 rpm, la velocidad es 20 * (0.5 + 1.5) = 40. También nos interesa controlar el consumo. Se parte de una base de 0.05 litros por kilómetro.
// A este valor se le aplican los siguientes ajustes: Si el motor está a más de 3000 rpm, entonces se multiplica por (rpm - 2500) / 500. P.ej., a 3500 rpm hay que multiplicar por 2, 
//a 4000 rpm por 3, etc. Si el motor está en primera, entonces se multiplica por 3. Si el motor está en segunda, entonces se multiplica por 2. Los efectos por revoluciones y 
//por cambio se acumulan. P.ej. si el motor está en primera y a 5000 rpm, entonces el consumo es 0.05 * 5 * 3 = 0.75 litros/km. 
//El metodo constructor debe entender estos mensajes: arrancar(), se pone en primera con 500 rpm. subirCambio() bajarCambio() subirRPM(cuantos) bajarRPM(cuantos) velocidad() 
//consumoActualPorKm()


function prototipo(){
    this.velocidad = 0
    this.consumo = 0

    this.arrancar = function(){
      this.cambio = 1
      this.RPM = 0
    }

    this.subirCambio = function(){
      if(this.cambio < 5){
         this.cambio = this.cambio + 1
      } else {
        console.log(`No se pueden subir mas cambios, el cambio es= ${this.cambio}`)
      }
    }

    this.bajarCambio = function(){
      if(this.cambio > 1){
         this.cambio = this.cambio - 1
      } else {
        console.log(`No se pueden bajar mas cambios. Cambio actual: ${this.cambio}`)
      }
    }

    this.subirRPM = function(valor){
      if(this.RPM < 5000){
         this.RPM = this.RPM + valor
      } else {
        console.log(`RPM actuales: ${this.RPM}`)
      }
    }

    this.bajarRPM = function(valor){
      if(this.RPM > 0){
         this.RPM = this.RPM - valor
      } else {
        console.log(`RPM actuales: ${this.RPM}`)
      }
    }

    this.velocidad = function(){
      this.velocidad = (this.RPM / 100) * (0.5 + (this.cambio / 2))
      console.log(`La velocidad actual es: ${this.velocidad}`)
    }

    this.consumoActualPorKm = function(){
      if(this.RPM <= 3000){
        this.consumo = 0.05
        console.log(`Consumo de ${this.consumo} litros por kilómetro`)
      }  else if(this.RPM > 3000 && this.RPM <= 3500){
         this.consumo = 0.05 * ((this.RPM - 2500) / 500) * 1
         console.log(`Consumo de ${this.consumo} litros por kilómetro`)
      } else if(this.RPM > 3500 && this.RPM <= 4000){
         this.consumo = 0.05 * ((this.RPM - 2500) / 500) * 2
         console.log(`Consumo de ${this.consumo} litros por kilómetro`)
      } else{
         this.consumo = 0.05 * ((this.RPM - 2500) / 500) * 3
         console.log(`Consumo de ${this.consumo} litros por kilómetro`)
      }
    }
}